# client

This is the initial (old) Frontend Repo for studidocs using Vue2. This Repo will not be updated any longer but is still used as reference. The current Frontend project is studidocs-client-vue3

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Compiles and minifies for production and serves the build files locally

```
npm run serve-prod
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```
