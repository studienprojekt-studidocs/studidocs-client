################################################################
#     BUILD                                  
################################################################

FROM node:12.18.3 as build
WORKDIR /app
COPY . .
RUN npm ci
ENV NODE_ENV="production"
RUN npm run build


################################################################
#     DEPLOY                                  
################################################################

FROM nginx:1.19.1-alpine as deploy

COPY nginx.conf /etc/nginx/nginx.conf

WORKDIR /usr/share/nginx/html
COPY --from=build /app/dist .

CMD ["nginx", "-g", "daemon off;"]
