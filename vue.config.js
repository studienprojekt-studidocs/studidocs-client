const path = require('path')

module.exports = {
  publicPath: '/studidocs-client',
  configureWebpack: {
    resolve: {
      alias: {
        colors: path.resolve(__dirname, './src/styles/colors.scss'), // '@import "colors"' can be used instead of relative path
        custom: path.resolve(__dirname, './src/styles/custom.scss'),
      },
    },
  },
}
