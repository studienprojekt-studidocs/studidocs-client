import Vue from 'vue'

import App from '@/App.vue'
import router from '@/router'

import messages from './lang'
import VueI18n from 'vue-i18n'

import './styles/main.scss'

import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faAngleLeft,
  faAngleRight,
  faFilter,
  faUpload,
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import IDGenerator from './utils/idGenerator'
import store from './store'

library.add(faAngleLeft, faAngleRight, faFilter, faUpload)
Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.use(IDGenerator)
Vue.use(VueI18n)

/*
global defintion because of often use 
 */
import Icon from '@/components/utils/Icon.vue'
import InfoBox from '@/components/utils/InfoBox.vue'
Vue.component('Icon', Icon)
Vue.component('InfoBox', InfoBox)

export const i18n = new VueI18n({
  locale: 'de',
  fallbackLocale: 'de',
  messages,
})

Vue.prototype.$eventHub = new Vue() // Global event bus

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount('#app')
