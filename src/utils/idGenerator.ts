import Vue, { VueConstructor } from 'vue'

/**
 * Creates a random id that is not already used in the DOM.
 */
function generateID() {
  let id: string = ''
  // it is unlikely that this produces an existing id, but it is not impossible
  do {
    // pseudo random number converted to base 36 string (contains letters a-z and 0-9)
    id = Math.random().toString(36)
  } while (document.getElementById(id) != null)
  return id
}

// export as Vue.js Plugin
// https://vuejs.org/v2/guide/plugins.html#Writing-a-Plugin
export default {
  install(Vue: VueConstructor) {
    Vue.prototype.$idGenerator = { generateID }
  },
}
