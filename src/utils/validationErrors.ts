export class ValidationErrors {
  private errors: Error[] = []

  public add(e: Error): void {
    this.errors.push(e)
  }

  public has(field?: string): boolean {
    if (field) {
      return this.errors.some((e) => e.field === field)
    } else {
      // field is not specified
      return this.errors.length !== 0
    }
  }

  public first(field?: string): string {
    if (field) {
      let first = this.errors.find((e) => e.field === field)
      // 'first' could be undefined
      return first ? first.msg : ''
    } else {
      // field is not specified
      return this.errors[0].msg
    }
  }

  public all(field?: string): Error[] {
    if (field) {
      return this.errors.filter((e) => e.field === field)
    } else {
      // field is not specified
      return this.errors
    }
  }

  public clear(): void {
    this.errors = []
  }
}

export interface Error {
  field: string
  msg: string
}

export const DefaultMessages = {
  FIELD_REQUIRED: 'Dies ist ein Pflichtfeld.',
  WRONG_DATA: 'Die eingegebenen Daten sind fehlerhaft.',
}
