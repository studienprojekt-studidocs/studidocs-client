export class UserLogin {
  userName: string
  id: string

  constructor(userName: string, id: string) {
    this.id = id
    this.userName = userName
  }
}
