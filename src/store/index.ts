import Vue from 'vue'
import Vuex from 'vuex'

import { StateTypes } from './types'

import { setCookie, getCookie } from '@/utils/cookie.utils'
import { CookieState } from '@/types'
import { getDocuments, getOwnDocuments } from '@/services/api/documents.service'
import { getStudentAccounts } from '@/services/api/studentAccounts.service'

Vue.use(Vuex)

export default new Vuex.Store<StateTypes>({
  state: {
    user: {
      userName: '',
      userToken: '',
      userRole: 'commonUser',
      userId: '',
    },
    cookieState: { allowed: false, info: true },
    documents: [],
    ownDocuments: [],
    studentAccounts: [],
    searchWords: [],
  },
  getters: {
    getUserToken: (state) => {
      return state.user.userToken
    },
    getUserId: (state) => {
      return state.user.userId
    },
    getUserRole: (state) => {
      return state.user.userRole
    },
    getCookiesAllowed: (state) => {
      return state.cookieState.allowed
    },
    getCookiesInfoState: (state) => {
      return state.cookieState.info
    },
  },
  mutations: {
    setUser(state, user) {
      state.user = user
    },
    resetUser(state: StateTypes) {
      state.user = {
        userName: '',
        userToken: '',
        userRole: 'commonUser',
        userId: '',
      }
    },
    setCookieState(state: StateTypes, cookieState: CookieState) {
      state.cookieState = cookieState
    },
    setDocumentsState(state: StateTypes, documents: any[]) {
      state.documents = documents
    },
    setOwnDocumentsState(state: StateTypes, documents: any[]) {
      state.ownDocuments = documents
    },
    setStudentAccountsState(state: StateTypes, studentAccounts: any[]) {
      state.studentAccounts = studentAccounts
    },
  },
  actions: {
    setUserAction(
      { commit },
      data: {
        userName: string
        userToken: string
        userRole: string
        userId: string
      },
    ) {
      commit('setUser', {
        userName: data.userName,
        userToken: data.userToken,
        userRole: data.userRole,
        userId: data.userId,
      })
      //commit('setUser', {userName: data.userName, userToken: data.userToken, userRole: "admin"}) //only for development
    },
    resetUserAction({ commit }) {
      commit('resetUser')
    },
    initCookieState({ commit }) {
      if (getCookie('cookiesOn')) {
        commit('setCookieState', { allowed: true, info: false })
      } else {
        commit('setCookieState', { allowed: false, info: true })
      }
    },
    setCookiesState({ commit }, data: { cookies: boolean }) {
      commit('setCookieState', { allowed: data.cookies, info: false })
      if (data.cookies) {
        setCookie('cookiesOn', 'true')
      }
    },
    getDocumentsState({ commit }, skipFilter: number) {
      getDocuments(skipFilter).then((documents) => {
        commit('setDocumentsState', documents)
      })
    },
    getOwnDocumentsState({ commit }) {
      getOwnDocuments().then((documents) => {
        commit('setOwnDocumentsState', documents)
      })
    },
    getStudentAccountsState({ commit }) {
      getStudentAccounts().then((studentAccounts) => {
        commit('setStudentAccountsState', studentAccounts)
      })
    },
  },
})
