import { Document } from '@/model/document.model'
import { CookieState } from '@/types'
import { StudentAccount } from '@/model/studentAccount.model'
import { SearchWord } from '@/model/searchWord.model'

export interface StateTypes {
  user: {
    userName: string
    userToken: string
    userRole: string
    userId: string
  }
  cookieState: CookieState
  documents: Document[]
  ownDocuments: Document[]
  studentAccounts: StudentAccount[]
  searchWords: SearchWord[]
}
