export { StateTypes } from './store/types'

export interface NavBarButtonConfig {
  route: string
  btnText: string
  roles: string[]
}

declare module 'vue/types/vue' {
  interface Vue {
    $idGenerator: IDGenerator
  }
}

export interface CookieState {
  allowed: boolean
  info: boolean
}

interface IDGenerator {
  generateID(): string
}
