import Vue from 'vue'
import Router, { Route } from 'vue-router'

import auth from '@/services/auth.service'
import { getRoutesForRouter } from '@/services/router.service'
import { isSameRoute, hasUserRequiredRole } from '@/utils/router.utils'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [...getRoutesForRouter(), { path: '*', redirect: '/' }],
})

// always redirect to '/login' if there is no valid token (either no token at all or an invalid/expired token)
router.beforeEach((to, from, next) => {
  if (
    to.meta &&
    !auth.isTokenStillSet() &&
    !hasUserRequiredRole(to.meta.roles)
  ) {
    next({
      path: '/',
      query: { redirect: to.fullPath },
    })
  } else {
    next()
  }
})

// set document title for every route
router.beforeEach((to, from, next) => {
  document.title = to.meta.title || document.title
  next()
})

//handler for '/signout'
//not very elegant to do this here, but a redirect-function in the RouterOptions didn't work (issue #1330 in vue-router's github repo, you have to google it)
//issue: redirect function is allways called when router-link in navbar-button is rendered
router.beforeEach((to, from, next) => {
  if (isSameRoute(to, '/signout')) {
    auth.logout()
    next({ path: '/' })
  } else {
    next()
  }
})

export default router
