import dateFns from 'date-fns'
import dfLocaleDE from 'date-fns/locale/de'

export function formatDate(date: Date, format: string) {
  if (!date) return ''
  //if `format` is undefined, dateFns.format() will return a complete timestamp
  return dateFns.format(date, format, { locale: dfLocaleDE })
}
