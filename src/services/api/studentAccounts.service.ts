import Axios from 'axios'
import { baseUrl } from '@/utils/http.utils'
import { getAccessToken } from '../auth.service'
import store from '@/store'
import { StudentAccount } from '@/model/studentAccount.model'
import { reject } from 'lodash'

// parse a studentAccount or an array of studentAccounts
const parseStudentAccount = (s: any) =>
  new StudentAccount(
    s.expiryDate,
    s.docentId,
    s.studentId,
    s.id,
    s.releaseRequests,
    s.authWithUserLogin,
  )
function parseStudentAccounts(sy: any): StudentAccount[] {
  if (Array.isArray(sy)) {
    return sy.map((s: any) => parseStudentAccount(s))
  } else {
    return [parseStudentAccount(sy)]
  }
}

/**
 * Retrieves all studentAccounts.
 */
export function getStudentAccounts(): Promise<StudentAccount[]> {
  const requestConfig = {
    method: 'get',
    url: `${baseUrl}/studentAccounts/studentAccountInfo`,
    headers: {
      ...(getAccessToken() && { accessToken: store.getters.getUserToken }),
    },
  }

  return new Promise((resolve, reject) => {
    Axios(requestConfig)
      .then((response) => {
        resolve(parseStudentAccounts(response.data))
      })
      .catch((error) => {})
  })
}

export function getStudentAccountById(id: number): Promise<StudentAccount> {
  const requestConfig = {
    method: 'get',
    url: `${baseUrl}/studentAccounts/studentAccountById?id=${id}`,
    headers: {
      ...(getAccessToken() && { accessToken: store.getters.getUserToken }),
    },
  }

  return new Promise((resolve, reject) => {
    Axios(requestConfig)
      .then((response) => {
        resolve(parseStudentAccount(response.data))
      })
      .catch((error) => {})
  })
}

/**
 * Updates a studentAccount
 */

export function refreshStudentAccount(data: {
  expiryDate: string
  studentAccountId: string
}): Promise<StudentAccount> {
  const requestConfig = {
    method: 'post',
    url: `${baseUrl}/studentAccounts/refreshStudentAccount`,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...(getAccessToken() && { accessToken: store.getters.getUserToken }),
    },
    data: data,
  }

  return new Promise((resolve, reject) => {
    Axios(requestConfig)
      .then((response) => {
        resolve(response.data)
      })
      .catch((error) => {
        reject(error.response)
      })
  })
}

/**
 * Adds a studentAccount
 */

export function addStudentAccount(data: {
  expiryDate: string
  studentName: string
}): Promise<StudentAccount> {
  const requestConfig = {
    method: 'post',
    url: `${baseUrl}/studentAccounts/addStudentAccount`,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...(getAccessToken() && { accessToken: store.getters.getUserToken }),
    },
    data: data,
  }

  return new Promise((resolve, reject) => {
    Axios(requestConfig)
      .then((response) => {
        resolve(response.data)
      })
      .catch((error) => {
        reject(error.response)
      })
  })
}

/**
 * Deletes a studentAccount
 */

export function deleteStudentAccount(data: {
  studentAccountId: string
}): Promise<StudentAccount> {
  const requestConfig = {
    method: 'post',
    url: `${baseUrl}/studentAccounts/deleteStudentAccount`,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...(getAccessToken() && { accessToken: store.getters.getUserToken }),
    },
    data: data,
  }

  return new Promise((resolve, reject) => {
    Axios(requestConfig)
      .then((response) => {
        resolve(response.data)
      })
      .catch((error) => {
        reject(error.response)
      })
  })
}
