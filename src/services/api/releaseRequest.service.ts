import Axios from 'axios'
import { baseUrl } from '@/utils/http.utils'
import { getAccessToken } from '../auth.service'
import store from '@/store'
import { ReleaseRequest } from '@/model/releaseRequest.model'

/**
 * Answer releaseRequest.
 */
export function answerReleaseRequest(data: {
  requestId: string
  isDocumentAccepted: boolean
  message: string
}): Promise<ReleaseRequest> {
  const requestConfig = {
    method: 'post',
    url: `${baseUrl}/releaseRequests/answerRequest`,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...(getAccessToken() && { accessToken: store.getters.getUserToken }),
    },
    data: data,
  }

  return new Promise((resolve, reject) => {
    Axios(requestConfig)
      .then((response) => {
        resolve(response.data)
      })
      .catch((error) => {
        reject(error.response)
      })
  })
}
