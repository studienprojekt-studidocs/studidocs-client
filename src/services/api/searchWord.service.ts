import { SearchWord } from '@/model/searchWord.model'
import Axios from 'axios'
import { baseUrl } from '@/utils/http.utils'
import { getAccessToken } from '../auth.service'
import store from '@/store'

const parseSearchWord = (s: any) => new SearchWord(s.word, s.id)
function parseSearchWords(sy: any): SearchWord[] {
  if (Array.isArray(sy)) {
    return sy.map((s: any) => parseSearchWord(s))
  } else {
    return [parseSearchWord(sy)]
  }
}

export function getSearchWords(): Promise<SearchWord[]> {
  const requestConfig = {
    method: 'get',
    url: `${baseUrl}/searchWords/searchWordInfo`,
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      ...(getAccessToken() && { accessToken: store.getters.getUserToken }),
    },
  }

  return new Promise((resolve, reject) => {
    Axios(requestConfig)
      .then((response) => {
        resolve(parseSearchWords(response.data))
      })
      .catch((error) => {
        reject(error.response)
      })
  })
}
