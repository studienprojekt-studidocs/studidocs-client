import _ from 'lodash'
import dateFns from 'date-fns'
import store from '@/store'
import Axios from 'axios'
import { baseUrl } from '@/utils/http.utils'

// key for userToken in sessionstorage
const userTokenKey = 'userToken'

/**
 * Checks if there is a token entry in the sessionStorage, otherwise the stored user data gets deleted.
 */
export function isTokenStillSet(): boolean {
  let token = sessionStorage.getItem(userTokenKey) || ''
  if (token != '') {
    return true
  }
  return false
}

/**
 * Saves a token entry in the sessionStorage.
 * If there already is a token, the newer token overwrites the older token.
 */
function saveToken(token: string): void {
  if (store.state.cookieState.allowed)
    sessionStorage.setItem(userTokenKey, token)
}

/**
 * Deletes the token entry from sessionStorage.
 */
function deleteToken(): void {
  sessionStorage.removeItem(userTokenKey)
}

/**
 * Decodes a JSON Web Token (token entry) and returns it's payload as JSON object
 * @param token The token entry as string
 */
function decodeToken(token: string) {
  return JSON.parse(atob(token))
}

/**
 * Checks the token, if it is valid. When validation is possitiv it decodes the token entry and returns it's payload as JSON object
 * @returns object if token is valid
 */
export function getDecodedToken() {
  if (hasValidToken()) {
    let token = sessionStorage.getItem(userTokenKey) || ''
    return decodeToken(token)
  }
  return null
}

/**
 * Checks if a token entry is expired.
 * @returns true if the token's expiration date is exceeded or if the token has no expiration date.
 */
function isTokenExpired(token: string): boolean {
  let decoded = decodeToken(token)
  let hasExpProperty = decoded.hasOwnProperty('exp') //the 'exp' property stores the expiration date of the token
  if (!hasExpProperty) {
    //a valid token must have a 'exp' property
    return true
  }
  let expDate = dateFns.parse(decoded.exp)
  return !dateFns.isAfter(expDate, new Date())
}

/**
 * Checks if there is a token entry in the sessionStorage, which is not expired.
 */
export function hasValidToken(): boolean {
  let token = sessionStorage.getItem(userTokenKey) || ''
  return !_.isEmpty(token) && !isTokenExpired(token)
}

/**
 * Retrieves a the accessToken from sessionStorage.
 */
export function getAccessToken() {
  let token = getDecodedToken()
  if (token) {
    return token.accessToken
  } else {
    return null
  }
}

/**
 * Sends username and password to the server.
 * If the credentials are valid, the server sends a token entry. This token will be saved into the sessionStorage.
 */
export async function login(username: string, password: string): Promise<void> {
  const data = {
    username: username,
    password: password,
  }

  const requestConfig = {
    method: 'post',
    url: `${baseUrl}/userLogins/login`,
    data: data,
    headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
  }

  return new Promise((resolve, reject) => {
    Axios(requestConfig)
      .then((response) => {
        //success
        let token: {
          accessToken: string
          exp: Date
          userName: string
          userRole: string
          userId: string
        } = {
          accessToken: response.data.token,
          exp: dateFns.addSeconds(dateFns.parse(dateFns.endOfToday()), 0), //change time if not correct //TODO Franzi
          userName: response.data.userName,
          userRole: response.data.userRole,
          userId: response.data.userId,
        }
        saveToken(btoa(JSON.stringify(token)))
        store.dispatch('setUserAction', {
          userName: token.userName,
          userToken: token.accessToken,
          userRole: token.userRole,
          userId: token.userId,
        })
        resolve()
      })
      .catch((error) => {
        reject(error.response.data.error.code)
      })
  })
}

/**
 * Logs out the current user.
 * Deletes the token entry from sessionStorage.
 */
export function logout(): void {
  store.dispatch('resetUserAction')
  deleteToken()
}

export default {
  getAccessToken,
  isTokenStillSet,
  getDecodedToken,
  login,
  logout,
  hasValidToken,
}
