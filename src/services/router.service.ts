import { VueConstructor } from 'vue'
import { RouteConfig } from 'vue-router'
import _ from 'lodash'

import { NavBarButtonConfig } from '@/types'

import Documents from '@/views/Documents.vue'
import Imprint from '@/views/Imprint.vue'
import LandingPage from '@/views/LandingPage.vue'
import Login from '@/views/Login.vue'
import Conditions from '@/views/Conditions.vue'
import StudentAdministration from '@/views/StudentAdministration.vue'
import UserArea from '@/views/UserArea.vue'
import MobileCourseDocuments from '@/components/documents/MobileCourseDocuments.vue'

const routes: { [index: string]: Route } = {
  login: {
    route: '/login',
    component: Login,
    roles: ['commonUser'],
    shortText: 'Login',
    longText: 'Login',
  },
  documents: {
    route: '/documents',
    component: Documents,
    roles: [],
    shortText: 'Dokumentenübersicht',
    longText: 'Dokumentenübersicht',
  },
  mobileCourseDocuments: {
    route: '/documents',
    roles: [],
    component: MobileCourseDocuments,
  },
  studentadministration: {
    route: '/studentadministration',
    component: StudentAdministration,
    roles: ['docent', 'admin'],
    shortText: 'Studentenverwaltung',
    longText: 'Studentenverwaltung',
  },
  userarea: {
    route: '/userarea',
    component: UserArea,
    roles: ['student', 'admin'],
    shortText: 'Uploadverwaltung',
    longText: 'Verwaltung eigener Uploads',
  },
  imprint: {
    route: '/imprint',
    component: Imprint,
    roles: [],
    shortText: 'Impressum',
    longText: 'Impressum',
  },
  landing: {
    route: '/',
    component: LandingPage,
    roles: [],
    shortText: 'Home',
    longText: 'Home',
  },
  conditions: {
    route: '/conditions',
    component: Conditions,
    roles: [],
    shortText: 'Nutzungsbedingungen',
    longText: 'Nutzungsbedingungen',
  },
  signout: {
    route: '/signout',
    roles: ['student', 'docent', 'admin'],
    shortText: 'Abmelden',
    longText: 'Abmelden',
  },
}

interface Route {
  route: string
  component?: VueConstructor
  roles: string[]
  shortText?: string
  longText?: string
  children?: Route[]
}

/**
 * Get the document title for a route.
 */
function getRouteTitle(longText: string | undefined): string {
  const appTitle = 'StudiDocs'
  if (longText === undefined) {
    return appTitle
  } else {
    return `${appTitle} - ${longText}`
  }
}

/**
 * Returns all routes (except 'signout') as RouteConfig-objects
 */
export function getRoutesForRouter(): RouteConfig[] {
  return _.chain(routes)
    .map((r: Route) => ({
      path: r.route,
      component: r.component,
      meta: {
        title: getRouteTitle(r.shortText),
        name: r.longText,
        roles: r.roles,
      },
    }))
    .value()
}

/**
 * Returns all routes that should appear in the navbar.
 */
export function getRoutesForNavbar(): NavBarButtonConfig[][] {
  //item order in these arrays determine the order of buttons in navbar (first = top)
  const navbarRoutesTop = [
    'landing',
    'documents',
    'userarea',
    'studentadministration',
  ]
  const navbarRoutesBottom = ['login', 'signout']

  //function to map route-objects to NavBarButtonConfig-objects
  const navbarRoutesMapping = (r: Route) => ({
    route: r.route,
    btnText: r.shortText!,
    roles: r.roles,
  })

  let upperNavButtons = _.chain(routes)
    .pick(navbarRoutesTop)
    .map(navbarRoutesMapping)
    .value()
  let lowerNavButtons = _.chain(routes)
    .pick(navbarRoutesBottom)
    .map(navbarRoutesMapping)
    .value()

  return [upperNavButtons, lowerNavButtons]
}

export function getRoutesForMobileNavbar(): NavBarButtonConfig[] {
  const mobileNavBarRoutes = [
    'documents',
    'userarea',
    'studentadministration',
    'login',
    'signout',
  ]

  const navbarRoutesMapping = (r: Route) => ({
    route: r.route,
    btnText: r.shortText!,
    roles: r.roles,
  })

  let buttons = _.chain(routes)
    .pick(mobileNavBarRoutes)
    .map(navbarRoutesMapping)
    .value()

  return buttons
}
