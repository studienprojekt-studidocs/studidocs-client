/**
 * The EventService handles global events.
 * Anywhere in the program, you can subscribe to or publish events.
 * Subscribing to an event means supplying a callback function that is executed (in the original context) when this event is fired.
 * An event is identified by its id.
 */
class EventService {
  private subscribers: Subscription[] = []

  private random = Math.random()

  public getRandom() {
    return this.random
  }

  public publish(id: string, data: any = {}): void {
    this.subscribers.forEach((subscriber) => {
      if (subscriber.id == id) {
        subscriber.callback(data)
      }
    })
  }

  public subscribe(id: string, callback: (data: any) => void): void {
    this.subscribers.push({ id: id, callback: callback })
  }
}

export default new EventService()

export interface Subscription {
  id: string
  callback: Function
}
