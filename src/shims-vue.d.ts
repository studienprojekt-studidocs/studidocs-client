// Tell the TypeScript compiler that all .vue files have the same shape as the Vue constructor.
// This is only important for the build process.

declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}
